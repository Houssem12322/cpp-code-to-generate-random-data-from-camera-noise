C++ code using OpenCV library to generate random binary data from camera noises.
The camera streaming is processed using ISAAC PRNG to generate random series with good statistics proprties for cryptographic uses.

If you want to build this source code directly you should use Qt 5.9 or later 'https://www.qt.io/download' and install opencv in ubuntu or Linux operating system.

