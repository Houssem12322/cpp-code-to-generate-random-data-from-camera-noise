#include <time.h>
#include "opencv2/opencv.hpp"
#include <iostream>
#include <QFile>
#include <QStandardPaths>
#include <unistd.h>
#include <QTime>
#include <QDataStream>

using namespace std;
using namespace cv;

#define numIteration 1
#define strongKeyImages 1
#define randomSerieLength 100


uint f(uint a, uint i)
{

    switch(i & 3)
    {
        case 0 : return ((a<<13) | (a>>18)); break;
        case 1 : return ((a>>6) | (a<<25)); break;
        case 2 : return ((a<<2) | (a>>29)); break;
        default : return ((a>>16) | (a<<15));

    }
}

uint* isaac(uint *start, uint nbrI,uint *r)
{

    uint i, j, index=0;
    static uint x, a=456456, b=123123;
    static uchar c=0;

    for(i=0; i<nbrI; i++)
    {
        c = c+1;
        b = b+c;

        for(j=0; j<256; j++)
        {
            x = start[j];
            a = f(a,j) + start[(j+128) & 255];
            start[j] = a^b + start[((x<<29)|(x>>2)) & 255];
            r[index] = x + a^start[((start[j]<<21)|(start[j]>>10)) & 255];
            b = r[index];
            index++;

        }
    }

}


//************************************************************************


int main()
{

    VideoCapture vcap(0);
    vcap.set(CV_CAP_PROP_SETTINGS,1);
    vcap.set(3,1920);
    vcap.set(4,1080);

    if(!vcap.isOpened()){
             cout << "Error opening video stream or file" << endl;
             return -1;
      }

   uchar start[1024] ={0};
   uchar result[1024 * numIteration] = {0};
   uint k = 0;
   uint sizeGen = 0;

   QFile keyFile(QStandardPaths::writableLocation(QStandardPaths::DesktopLocation)+ "/coldDark.dat");
   if(!keyFile.open(QIODevice::WriteOnly | QIODevice::Append))
   {
       cout<<"ERROR: the file couldn't be opened";
       exit(20);
   }
   QDataStream out(&keyFile);

   while(sizeGen < 900){

       Mat frame;
       vcap >> frame;

       int channels = frame.channels();
       int nRows = frame.rows;
       int nCols = frame.cols * channels;
       if(frame.isContinuous())
       {
            nCols *= nRows;
            nRows = 1;
       }
       int i,j;
       uchar *p;
       for( i = 0; i < nRows; ++i)
       {
            p = frame.ptr<uchar>(i);
            for ( j = 0; j < nCols; ++j)
            {

                start[k] ^= p[j];
                if(k == 1023)
                {
                    isaac((uint*)start,numIteration,(uint*)result);
                    k=0;
                    sizeGen++;
                }
                k++;
            }
       }



   }
   sizeGen = 0;

   while(sizeGen < randomSerieLength){

       Mat frame;
       vcap >> frame;

       int channels = frame.channels();
       int nRows = frame.rows;
       int nCols = frame.cols * channels;
       if(frame.isContinuous())
       {
            nCols *= nRows;
            nRows = 1;
       }
       int i,j;
       uchar *p;
       for( i = 0; i < nRows; ++i)
       {
            p = frame.ptr<uchar>(i);
            for ( j = 0; j < nCols; ++j)
            {

                start[k] ^= p[j];
                k++;
                if(k == 1024)
                {
                    isaac((uint*)start,numIteration,(uint*)result);
                    out.writeRawData((char*)result,1024);
                    k=0;
                }

            }
       }

       sizeGen++;


   }


    return 0;
}

